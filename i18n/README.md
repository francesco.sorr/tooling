# i18n tooling

Provides common internalization tooling for KWinFT projects.

## Installing i18n-extractor

```
go get gitlab.com/kwinft/tooling/i18n/extractor
```
